import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity({ name: 'task_interaction' })
export class TaskInteraction {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column("varchar", { nullable: false, name: "task_attempt_id" })
    taskId: string;

    @Column("varchar", { nullable: false, name: "type" })
    type: string;

    @Column("varchar", { nullable: true, name: "outcome" })
    outcome: string;

    @Column("bigint", { nullable: false, name: "start_ts" })
    startTs: number;

    @Column("bigint", { nullable: true, name: "end_ts" })
    endTs: number;
}