import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity({ name: 'task_attempt' })
export class TaskAttempt {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column("varchar", { nullable: false, name: "session_id" })
    sessionId: string;

    @Column("varchar", { nullable: false, name: "task_name" })
    taskName: string;

    // @ManyToMany(() => LabSession)
    // labSession: LabSession;

    @Column("bigint", { nullable: false, name: "start_ts" })
    startTs: number;

    @Column("bigint", { nullable: true, name: "end_ts" })
    endTs: number;
}