import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'lab_session' })
export class LabSession {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar', { nullable: false, name: 'person_id' })
  personId: string;

  @Column('boolean', { nullable: false, name: 'webcam_capture_consent' })
  webcamCaptureConsent: boolean;

  @Column('bigint', { nullable: false, name: 'start_ts' })
  startTs: number;

  @Column('bigint', { nullable: true, name: 'end_ts' })
  endTs: number;

  @Column('varchar', { nullable: false, name: 'video_file_id' })
  videoFileId: string;

  @Column('bigint', { nullable: true, name: 'video_start_ts' })
  videoStartTs: number;

  @Column('bigint', { nullable: true, name: 'video_end_ts' })
  videoEndTs: number;
}
