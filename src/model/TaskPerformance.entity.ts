import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'task_performance' })
export class TaskPerformance {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar', { nullable: false, name: 'task_attempt_id' })
  taskAttemptId: string;

  @Column('boolean', { nullable: false, name: 'completed' })
  completed: boolean;

  @Column('integer', { nullable: false, name: 'duration' })
  duration: number;

  @Column('varchar', { nullable: true, name: 'outcome' })
  outcome: string;

  @Column('smallint', { nullable: true, name: 'score' })
  score: number;
}
