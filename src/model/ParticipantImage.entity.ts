import { Column, Entity, Long, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'participant_image' })
export class ParticipantImage {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar', { nullable: false, name: 'session_id' })
  sessionId: string;

  @Column('integer', { nullable: false, name: 'frame_number' })
  frameNumber: number;

  @Column('bytea', { nullable: false, name: 'shot' })
  shot: Uint8Array;

  @Column('bigint', { nullable: false, name: 'timestamp' })
  timestamp: number;
}
