import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({ name: 'person' })
export class Person {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column("varchar", { nullable: false, name: "username" })
    username: string;

    @Column("int", { nullable: true, name: "year_of_birth" })
    yearOfBirth: number;

    @Column("varchar", { nullable: true, name: "gender" })
    gender: string;

    @Column("varchar", { nullable: true, name: "race" })
    race: string;
}