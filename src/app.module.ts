import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { configService } from './config/config.service';
import { ObservationModule } from './observation/observation.module';
import { ParticipantImage } from './model/ParticipantImage.entity';
import { Person } from './model/Person.entity';
import { LabSession } from './model/LabSession.entity';
import { TaskAttempt } from './model/TaskAttempt.entity';
import { TaskInteraction } from './model/TaskInteraction.entity';
import { TaskPerformance } from './model/TaskPerformance.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'postgres',
      password: '******',
      database: 'db-sandbox1',
      entities: [
        ParticipantImage,
        Person,
        LabSession,
        TaskAttempt,
        TaskInteraction,
        TaskPerformance,
      ],
      synchronize: false,
      dropSchema: false,
    }),
    ObservationModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
