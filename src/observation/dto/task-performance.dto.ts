export class TaskPerformanceDto {
  taskAttemptId: string;
  completed: boolean;
  duration: number;
  outcome: string;
  score: number;
}
