export class TaskInteractionDto {
    id?: string;
    taskId: string;
    type: string;
    outcome?: string;
    startTs: number;
    endTs?: number;
}