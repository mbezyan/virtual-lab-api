export class TaskAttemptDto {
    sessionId: string;
    taskName: string;
}