export class ParticipantImageDto {
  frameNumber: number;
  timestamp: number;
  image: string;
}
