export default class PersonDto {
    username: string;
    yearOfBirth: number;
    gender: string;
    race: string;
}