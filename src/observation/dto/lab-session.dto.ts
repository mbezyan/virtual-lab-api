export class LabSessionDto {
    personId: string;
    webcamCaptureConsent: boolean;
    endTs: number;
    videoFileId: string;
    videoStartTs: number;
    videoEndTs: number;
}