import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Person } from '../model/Person.entity';
import { ParticipantImage } from '../model/ParticipantImage.entity';

import { ObservationController } from './controllers/observation.controller';

import { ObservationService } from './observation.service';
import { PersonController } from './controllers/person.controller';
import { LabSession } from '../model/LabSession.entity';
import { LabSessionsController } from './controllers/lab-sessions.controller';
import { TaskAttempt } from '../model/TaskAttempt.entity';
import { TaskAttemptsController } from './controllers/task-attempts.controller';
import { TaskInteractionsController } from './controllers/task-interactions.controller';
import { TaskInteraction } from '../model/TaskInteraction.entity';
import { TaskPerformance } from '../model/TaskPerformance.entity';
import { TaskPerformancesController } from './controllers/task-performances.controller';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ParticipantImage,
      Person,
      LabSession,
      TaskAttempt,
      TaskInteraction,
      TaskPerformance,
    ]),
  ],
  providers: [ObservationService],
  controllers: [
    ObservationController,
    PersonController,
    LabSessionsController,
    TaskAttemptsController,
    TaskInteractionsController,
    TaskPerformancesController,
  ],
})
export class ObservationModule {}
