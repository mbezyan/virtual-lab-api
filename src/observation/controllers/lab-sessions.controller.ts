import { Body, Controller, HttpCode, Post, Logger, Put, Param } from "@nestjs/common";
import { LabSessionDto } from "../dto/lab-session.dto";
import { ObservationService } from "../observation.service";

@Controller('lab-sessions')
export class LabSessionsController {
    private readonly logger = new Logger(LabSessionsController.name);

    constructor(private readonly observationService: ObservationService){}

    @Post()
    @HttpCode(201)
    async createLabSession(@Body() labSessionDto: LabSessionDto) {
        const FN = 'createLabSession()';
        this.logger.log(`${FN} <== ${JSON.stringify(labSessionDto)}`);        
        const id: string = await this.observationService.createLabSession(labSessionDto);
        this.logger.log(`${FN} ==> lab session ${id}`);
        return id;
    }

    @Put(':id')
    @HttpCode(200)
    async updateTaskAttempt(@Param('id') id: string, @Body() labSessionDto: LabSessionDto) {
        const FN = 'updateTaskAttempt()';
        this.logger.log(`${FN} <== ${JSON.stringify(labSessionDto)}`);        
        labSessionDto = await this.observationService.updateLabSession(id, labSessionDto);
        this.logger.log(`${FN} ==> ${JSON.stringify(labSessionDto)}`);
        return labSessionDto;
    }
}