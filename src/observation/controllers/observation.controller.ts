import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Res,
  Logger,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { LabSessionDto } from '../dto/lab-session.dto';
import { ParticipantImageDto } from '../dto/participant-image.dto';
import { ObservationService } from '../observation.service';

class SaveImagesRequest {
  sessionId: string;
  images: ParticipantImageDto[];
}

class SaveVideoRequest {
  sessionId: string;
  video;
  startTs: number;
}

@Controller('observations')
export class ObservationController {
  private readonly logger = new Logger(ObservationController.name);

  constructor(private readonly observationService: ObservationService) {}

  @Post()
  @HttpCode(201)
  async saveImages(@Body() saveImagesRequest: SaveImagesRequest) {
    const FN = 'saveImages()';
    this.logger.log(
      `${FN} <== ${saveImagesRequest.sessionId}: ${saveImagesRequest.images.length} images`,
    );
    const ids: string[] = await this.observationService.saveImages(
      saveImagesRequest.sessionId,
      saveImagesRequest.images,
    );
    this.logger.log(`${FN} ==> with ${ids?.length} ids`);
    return ids;
  }

  @Post('videos')
  @UseInterceptors(
    FileInterceptor('video', {
      dest: './upload',
    }),
  )
  @HttpCode(201)
  async saveVideo(
    @UploadedFile() file: Express.Multer.File,
    @Body('sessionId') sessionId: string,
    @Body('startTs') startTs: number,
    @Body('endTs') endTs: number,
  ): Promise<LabSessionDto> {
    const FN = 'saveVideo()';
    this.logger.log(
      `${FN} <== ${sessionId}: ${startTs}-${endTs} ${file.path} (${file.size})`,
    );
    let labSessionDto: LabSessionDto = new LabSessionDto();
    labSessionDto.videoFileId = file.filename;
    labSessionDto.videoStartTs = startTs;
    labSessionDto.videoEndTs = endTs;
    labSessionDto = await this.observationService.updateLabSession(
      sessionId,
      labSessionDto,
    );
    this.logger.log(`${FN} ==> ${JSON.stringify(labSessionDto)}`);
    return labSessionDto;
  }
}
