import { Body, Controller, HttpCode, Post, Logger, Put, Param } from "@nestjs/common";
import { LabSessionDto } from "../dto/lab-session.dto";
import { TaskAttemptDto } from "../dto/task-attempt.dto";
import { ObservationService } from "../observation.service";

@Controller('task-attempts')
export class TaskAttemptsController {
    private readonly logger = new Logger(TaskAttemptsController.name);

    constructor(private readonly observationService: ObservationService){}

    @Post()
    @HttpCode(201)
    async createTaskAttempt(@Body() taskAttemptDto: TaskAttemptDto) {
        const FN = 'createTaskAttempt()';
        this.logger.log(`${FN} <== ${JSON.stringify(taskAttemptDto)}`);        
        const id: string = await this.observationService.createTaskAttempt(taskAttemptDto);
        this.logger.log(`${FN} ==> task attempt ${id} created`);
        return id;
    }

    @Put(':id')
    @HttpCode(200)
    async updateTaskAttempt(@Param('id') id: string, @Body() taskAttemptDto: TaskAttemptDto) {
        const FN = 'updateTaskAttempt()';
        this.logger.log(`${FN} <== ${JSON.stringify(taskAttemptDto)}`);        
        taskAttemptDto = await this.observationService.updateTaskAttempt(id, taskAttemptDto);
        this.logger.log(`${FN} ==> task attempt ${id} updated`);
        return taskAttemptDto;
    }
}