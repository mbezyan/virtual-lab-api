import { Body, Controller, HttpCode, Post, Logger } from "@nestjs/common";
import PersonDto from "../dto/person.dto";
import { ObservationService } from "../observation.service";

@Controller('persons')
export class PersonController {
    private readonly logger = new Logger(PersonController.name);

    constructor(private readonly observationService: ObservationService){}

    @Post()
    @HttpCode(201)
    async createPerson(@Body() personDto: PersonDto) {
        const FN = 'createPerson()';
        this.logger.log(`${FN} ==> with ${JSON.stringify(personDto)}`);        
        const id: string = await this.observationService.createPerson(personDto);
        this.logger.log(`${FN} <== with person ${id} created.`);
        return id;
    }
}