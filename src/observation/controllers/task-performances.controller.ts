import {
  Body,
  Controller,
  HttpCode,
  Post,
  Logger,
  Put,
  Param,
} from '@nestjs/common';
import { TaskPerformanceDto } from '../dto/task-performance.dto';
import { ObservationService } from '../observation.service';

@Controller('task-performances')
export class TaskPerformancesController {
  private readonly logger = new Logger(TaskPerformancesController.name);

  constructor(private readonly observationService: ObservationService) {}

  @Post()
  @HttpCode(201)
  async createTaskPerformance(@Body() taskPerformanceDto: TaskPerformanceDto) {
    const FN = 'createTaskAttempt()';
    this.logger.log(`${FN} <== ${JSON.stringify(taskPerformanceDto)}`);
    const id: string = await this.observationService.createTaskPerformance(
      taskPerformanceDto,
    );
    this.logger.log(`${FN} ==> Task performance ID ${id}`);
    return id;
  }
}
