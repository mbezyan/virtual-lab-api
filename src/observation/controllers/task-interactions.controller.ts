import { Body, Controller, HttpCode, Post, Logger, Put, Param } from "@nestjs/common";
import { TaskInteractionDto } from "../dto/task-interaction.dto";
import { ObservationService } from "../observation.service";

@Controller('task-interactions')
export class TaskInteractionsController {
    private readonly logger = new Logger(TaskInteractionsController.name);

    constructor(private readonly observationService: ObservationService){}

    @Post()
    @HttpCode(201)
    async upsertTaskInteractions(@Body() taskInteractionDtos: TaskInteractionDto[]) {
        const FN = 'upsertTaskInteractions()';
        this.logger.log(`${FN} <== ${taskInteractionDtos.length} task interactions`);        
        const ids: string[] = await this.observationService.upsertTaskInteractions(taskInteractionDtos);
        this.logger.log(`${FN} ==> ${ids.length} task interactions upserted`);
        return ids;
    }
}