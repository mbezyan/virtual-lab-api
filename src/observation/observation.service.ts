import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ParticipantImage } from '../model/ParticipantImage.entity';
import { Repository } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';
import { encode, decode } from 'uint8-to-base64';
import { ParticipantImageDto } from './dto/participant-image.dto';
import { Person } from '../model/Person.entity';
import PersonDto from './dto/person.dto';
import { LabSessionDto } from './dto/lab-session.dto';
import { LabSession } from '../model/LabSession.entity';
import { TaskAttemptDto } from './dto/task-attempt.dto';
import { TaskAttempt } from '../model/TaskAttempt.entity';
import { TaskInteractionDto } from './dto/task-interaction.dto';
import { TaskInteraction } from '../model/TaskInteraction.entity';
import { TaskPerformance } from '../model/TaskPerformance.entity';
import { TaskPerformanceDto } from './dto/task-performance.dto';

@Injectable()
export class ObservationService {
  private readonly logger = new Logger(ObservationService.name);

  constructor(
    @InjectRepository(ParticipantImage)
    private participantImageRepository: Repository<ParticipantImage>,
    @InjectRepository(Person)
    private personRepository: Repository<Person>,
    @InjectRepository(LabSession)
    private labSessionRepository: Repository<LabSession>,
    @InjectRepository(TaskAttempt)
    private taskAttemptRepository: Repository<TaskAttempt>,
    @InjectRepository(TaskInteraction)
    private taskInteractionRepository: Repository<TaskInteraction>,
    @InjectRepository(TaskPerformance)
    private taskPerformanceRepository: Repository<TaskPerformance>,
  ) {}

  async saveImages(
    sessionId: string,
    images: ParticipantImageDto[],
  ): Promise<string[]> {
    const FN: string = 'saveImages()';
    this.logger.debug(`${FN} <== ${sessionId} ${images.length} images`);
    const imageIds: string[] = [];

    await Promise.all(
      images.map(async (image) => {
        const particpantImage: ParticipantImage = new ParticipantImage();
        particpantImage.id = generateUUID();
        particpantImage.sessionId = sessionId;
        particpantImage.frameNumber = image.frameNumber;
        particpantImage.shot = Buffer.from(image.image, 'base64');
        particpantImage.timestamp = image.timestamp;
        await this.participantImageRepository.save(particpantImage);
        imageIds.push(particpantImage.id);
        this.logger.verbose(`${FN} Persisted ${particpantImage.id}`);
      }),
    );

    this.logger.debug(
      `${FN} <== with ${imageIds?.length} participant image ids`,
    );
    return imageIds;
  }

  async readImage(id: string): Promise<string> {
    const image: ParticipantImage =
      await this.participantImageRepository.findOne(id);
    return encode(image.shot);
  }

  async createPerson(personDto: PersonDto): Promise<string> {
    const FN: string = 'createPerson()';
    this.logger.debug(`${FN} ==> with ${JSON.stringify(personDto)}`);
    const person: Person = new Person();
    person.id = generateUUID();
    person.username = personDto.username;
    person.yearOfBirth = personDto.yearOfBirth;
    person.gender = personDto.gender;
    person.race = personDto.race;
    await this.personRepository.save(person);
    this.logger.verbose(`${FN} Persisted ${person.id}`);
    return person.id;
  }

  async createLabSession(labSessionDto: LabSessionDto): Promise<string> {
    const FN: string = 'createLabSession()';
    this.logger.debug(`${FN} <== ${JSON.stringify(labSessionDto)}`);
    const labSession: LabSession = new LabSession();
    labSession.id = generateUUID();
    labSession.personId = labSessionDto.personId;
    labSession.webcamCaptureConsent = labSessionDto.webcamCaptureConsent;
    labSession.startTs = Date.now();
    await this.labSessionRepository.save(labSession);
    this.logger.verbose(`${FN} Persisted ${labSession.id}`);
    return labSession.id;
  }

  async updateLabSession(
    id: string,
    labSessionDto: LabSessionDto,
  ): Promise<LabSessionDto> {
    const FN: string = 'updateLabSession()';
    this.logger.debug(`${FN} <== ${JSON.stringify(labSessionDto)}`);
    let labSession: LabSession = await this.labSessionRepository.findOne(id);
    labSessionDto.personId && (labSession.personId = labSessionDto.personId);
    labSessionDto.webcamCaptureConsent &&
      (labSession.webcamCaptureConsent = labSessionDto.webcamCaptureConsent);
    labSessionDto.videoFileId &&
      (labSession.videoFileId = labSessionDto.videoFileId);
    labSessionDto.videoStartTs &&
      (labSession.videoStartTs = labSessionDto.videoStartTs);
    labSessionDto.videoEndTs &&
      (labSession.videoEndTs = labSessionDto.videoEndTs);
    labSessionDto.endTs && (labSession.endTs = labSessionDto.endTs);
    labSession = await this.labSessionRepository.save(labSession);
    this.logger.verbose(`${FN} Persisted ${labSession.id}`);
    labSessionDto = mapLabSessionEntityToDto(labSession);
    this.logger.debug(`${FN} ==> ${JSON.stringify(labSessionDto)}`);
    return labSessionDto;
  }

  async createTaskAttempt(taskAttemptDto: TaskAttemptDto): Promise<string> {
    const FN: string = 'createTaskAttempt()';
    this.logger.debug(`${FN} <== ${JSON.stringify(taskAttemptDto)}`);
    const taskAttempt: TaskAttempt = new TaskAttempt();
    taskAttempt.id = generateUUID();
    taskAttempt.sessionId = taskAttemptDto.sessionId;
    taskAttempt.taskName = taskAttemptDto.taskName;
    taskAttempt.startTs = Date.now();
    await this.taskAttemptRepository.save(taskAttempt);
    this.logger.verbose(`${FN} Persisted ${taskAttempt.id}`);
    return taskAttempt.id;
  }

  async updateTaskAttempt(
    id: string,
    taskAttemptDto: TaskAttemptDto,
  ): Promise<TaskAttemptDto> {
    const FN: string = 'updateTaskAttempt()';
    this.logger.debug(`${FN} <== ${JSON.stringify(taskAttemptDto)}`);
    let taskAttempt: TaskAttempt = await this.taskAttemptRepository.findOne(id);
    taskAttemptDto.sessionId &&
      (taskAttempt.sessionId = taskAttemptDto.sessionId);
    taskAttemptDto.taskName && (taskAttempt.taskName = taskAttemptDto.taskName);
    taskAttempt.endTs = Date.now();
    taskAttempt = await this.taskAttemptRepository.save(taskAttempt);
    this.logger.verbose(`${FN} Updated ${taskAttempt.id}`);
    return mapTaskAttemptEntityToDto(taskAttempt);
  }

  async upsertTaskInteractions(
    taskInteractionDtos: TaskInteractionDto[],
  ): Promise<string[]> {
    const FN: string = 'upsertTaskInteractions()';
    this.logger.debug(
      `${FN} <== ${taskInteractionDtos.length} task interactions`,
    );
    const taskInteractionIds: string[] = [];

    await Promise.all(
      taskInteractionDtos.map(async (taskInteractionDto) => {
        let taskInteraction: TaskInteraction =
          await this.taskInteractionRepository.findOne(taskInteractionDto.id);
        if (taskInteraction) {
          // Update
          taskInteractionDto.type &&
            (taskInteraction.type = taskInteractionDto.type);
          taskInteractionDto.taskId &&
            (taskInteraction.taskId = taskInteractionDto.taskId);
          taskInteractionDto.startTs &&
            (taskInteraction.startTs = taskInteractionDto.startTs);
        } else {
          // Insert
          taskInteraction = new TaskInteraction();
          taskInteraction.id = taskInteractionDto.id;
          taskInteraction.type = taskInteractionDto.type;
          taskInteraction.taskId = taskInteractionDto.taskId;
          taskInteraction.startTs = taskInteractionDto.startTs;
        }
        taskInteractionDto.outcome &&
          (taskInteraction.outcome = taskInteractionDto.outcome);
        taskInteractionDto.endTs &&
          (taskInteraction.endTs = taskInteractionDto.endTs);

        await this.taskInteractionRepository.save(taskInteraction);
        taskInteractionIds.push(taskInteraction.id);
        this.logger.verbose(`${FN} Upserted ${taskInteraction.id}`);
      }),
    );

    this.logger.debug(
      `${FN} ==> Upserted ${taskInteractionIds?.length} task interaction.`,
    );
    return taskInteractionIds;
  }

  async createTaskPerformance(
    taskPerformanceDto: TaskPerformanceDto,
  ): Promise<string> {
    const FN: string = 'createTaskPerformance()';
    this.logger.debug(`${FN} <== ${JSON.stringify(taskPerformanceDto)}`);
    const taskPerformance: TaskPerformance = new TaskPerformance();
    taskPerformance.id = generateUUID();
    taskPerformance.taskAttemptId = taskPerformanceDto.taskAttemptId;
    taskPerformance.completed = taskPerformanceDto.completed;
    taskPerformance.duration = taskPerformanceDto.duration;
    taskPerformance.outcome = taskPerformanceDto.outcome;
    taskPerformance.score = taskPerformanceDto.score;
    await this.taskPerformanceRepository.save(taskPerformance);
    this.logger.verbose(`${FN} Persisted ${taskPerformance.id}`);
    return taskPerformance.id;
  }
}

function generateUUID(): string {
  return uuidv4().replaceAll('-', '');
}

function mapTaskAttemptEntityToDto(taskAttempt: TaskAttempt): TaskAttemptDto {
  const taskAttemptDto: TaskAttemptDto = new TaskAttemptDto();
  taskAttemptDto.sessionId = taskAttempt.sessionId;
  taskAttemptDto.taskName = taskAttempt.taskName;
  return taskAttemptDto;
}
function mapLabSessionEntityToDto(labSession: LabSession): LabSessionDto {
  const labSessionDto = new LabSessionDto();
  labSessionDto.endTs = labSession.endTs;
  labSessionDto.personId = labSession.personId;
  labSessionDto.webcamCaptureConsent = labSession.webcamCaptureConsent;
  labSessionDto.videoFileId = labSession.videoFileId;
  labSessionDto.videoStartTs = labSession.videoStartTs;
  labSessionDto.videoEndTs = labSession.videoEndTs;
  return labSessionDto;
}
